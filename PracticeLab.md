# Assignment Overview

<center>
    <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DV0101EN-SkillsNetwork/labs/Module%204/logo.png" width="300" alt="cognitiveclass.ai logo" />
</center>

- [Story](#Story)
- [Components of the Dashboard](#Components of the  items)
- [Expected layout](#expected-layout)
- [Requirements to create the dashboard](#Requirements to create the expected result)
- [Hints to complete TODOs](#Hints to complete TASKS)

**Estimated time needed:** 45 minutes

# About Skills Network Cloud IDE

This Skills Network Labs Cloud IDE (Integrated Development Environment) provides a hands-on environment in your web browser for completing course and project related labs. It utilizes Theia, an open-source IDE platform, that can be run on desktop or on the cloud.
So far in the course you have been using Jupyter notebooks to run your python code. This IDE provides an alternative for editing and running your Python code. In this lab you will be using this alternative Python runtime to create and launch your Dash applications.

### Important Notice about this lab environment

Please be aware that sessions for this lab environment are not persisted. When you launch the Cloud IDE, you are presented with a 'dedicated computer on the cloud' exclusively for you. This is available to you as long as you are actively working on the labs.

Once you close your session or it is timed out due to inactivity,  you are logged off, and this 'dedicated computer on the cloud' is deleted along with any files you may have created, dowloaded or installed.  The next time you launch this lab, a new environment is created for you.

*If you finish only part of the lab and return later, you may have to start from the beginning. So, it is a good idea to plan to your time accordingly and finish your labs in a single session.*

## Story:

Here we are looking into an **autombile dataset** which has various attributes like **drive-wheels,body-style and price**.

The possible values of drive-wheels are **4 wheel Drive(4wd),Front WheelDrive(fwd) and Rear wheel Drive(rwd)**.

The different body styles of the cars are **hardtop,sedan,convertible**and so on.

A  customer wants to purchase the cars with less price , different body styles and wants to look for the wheel drive with this arrangement.

As a data analyst, you have been given a task to visually show the body-style and prices with respect for each wheel drive selected.

So ideally you want to showcase this in the form of 2 interactive charts such as pie chart and bar chart on selection of wheeldrives.

Below are the key  items,

   - Drive wheels 
   

_NOTE:_ Drive wheels having different options such as Four Wheel Drive, Rear Wheel Drive and Front Wheel Drive.

## Components of the  items

1. Drive Wheel Type

   For the chosen Drive wheel,
   
   - Pie Chart showing body style and price.
   - Bar Chart showing body style and price.
   
   


## Expected Layout

![](images/Layout1.jpg)

## Requirements to create the expected result

- A dropdown [menu](https://dash.plotly.com/dash-core-components/dropdown): For choosing Drive wheel type
- The dropdown will be designed as follows:
- An outer division with two inner divisions (as shown in the expected layout) 
- One of the inner divisions will have information about the dropdown(which is the input) and the other one is for adding graphs(the 2 output   graphs).
- Callback function to compute data, create graph and return to the layout.


### To do:

1.  Import required libraries and read the dataset
2.  Create an application layout
3.  Add title to the dashboard using HTML H1 component
4.  Add a dropdown using dcc.dropdown
5.  Add the pie chart and bar chart core graph components.
6.  Run the app

# Get the tool ready


- Open a new terminal, by clicking on the menu bar and selecting **Terminal**->**New Terminal**, as in the image below.

![](images/terminal.png)

- Now, you have a  terminal ready to start the lab.

![](images/full.png)


# Get the application skeleton


- Copy and paste the command in the terminal to download the csv.
```

wget https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DA0101EN-SkillsNetwork/labs/Data%20files/automobileEDA.csv
```





![](images/get_file.png)




You can use this as a base code to complete the task below.


# Let's create the application

- Create a new file called 'Dash_Auto.py'


- Copy the code mentioned in the skeleton file and save it.

## Structure of the skeleton file

```
import pandas as pd
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
import plotly.express as px
from dash import no_update

app = dash.Dash(__name__)

# REVIEW1: Clear the layout and do not display exception till callback gets executed
app.config.suppress_callback_exceptions = True

# Read the automobiles data into pandas dataframe
auto_data =  pd.read_csv('automobileEDA.csv', 
                            encoding = "ISO-8859-1",
                            )

#Layout Section of Dash

app.layout = html.Div(children=[#TASK1

     #outer division starts
     html.Div([
                   # First inner divsion for  adding dropdown helper text for Selected Drive wheels
                    html.Div(
                            #TASK2
                     ),
                    #Second Inner division for adding 2 inner divisions for 2 output graphs 

                    #TASK3
                    html.Div([
                
                        #TASK4
                        
                    ], style={'display': 'flex'}),


    ])
    #outer division ends

])
#layout ends

#Place to add @app.callback Decorator
#TASK5

   
#Place to define the callback function .
#TASK6
   
   


if __name__ == '__main__':
    app.run_server()
    




```

# Hints to complete TASKS

Search/Look for `TASK` word in the script to identify places where you need to complete the code.


# TASK 1: Add title to the dashboard
Update the `html.H1()` tag to hold the application title. 

- Application title is `Car Automobile Components`
- Use style parameter provided below to make the title `center` aligned, with color code `#503D36`, and font-size as `24`

```
'Car Automobile Components',
style={'textAlign': 'center', 'color': '#503D36', 'font-size': 24}
```
After updating the `html.H1()` with the application title, the `app.layout` will look like:

![](images/h1.png)


Reference [link](https://dash.plotly.com/dash-html-components/h1)
          [link](https://dash.plotly.com/dash-html-components)

# TASK 2: Add a Label to the dropdown 

   - Use the  `html.H2()` tag to hold the  label for the dropdown inside the first inner division 
      
      - Label is `Drive Wheels Type:`
      - Use style parameter provided below to allign the label  `margin-right` with value  `2em` which means 2 times the size of the current font.

   
   ```
   html.H2('Drive Wheels Type:', style={'margin-right': '2em'}),
   
   ```

   After updating the label the `app.layout` will now look like this

![](images/h2.png)
# TASK 3: Next lets add the dropdown right below the first inner division.

 - The dropdown has an `id` as `demo-dropdown`.
 - These options have the  labels as  `Rear Wheel Drive` ,`Front Wheel Drive` and `Four Wheel Drive`
 - The values allowed in the dropdown are `rwd`,`fwd`,`4wd`
 - The default value when the dropdown is displayed is  `rwd`.


    
```
  dcc.Dropdown(
                            id='demo-dropdown',
                        options=[
                             {'label': 'Rear Wheel Drive', 'value': 'rwd'},
                            {'label': 'Front Wheel Drive', 'value': 'fwd'},
                             {'label': 'Four Wheel Drive', 'value': '4wd'}
        ],
        value='rwd'
```



Reference [link](https://dash.plotly.com/dash-core-components/dropdown)

Once you add the dropdown the '`app.layout` will appear as follows

![](images/dropdown.png)

# TASK 4: Add two empty divisions for output inside the next inner division . 
-  Use 2 `html.Div()` tags . 

-  Provide division ids as `plot1` and `plot2`.

  ```
  html.Div([ ], id='plot1'),
  html.Div([ ], id='plot2')
```
Once you add the divisions the '`app.layout` will appear as follows

![](images/plot1plot2.png)



# TASK 5: Add the Ouput and input components inside the app.callback decorator.

- The `inputs` and `outputs` of our application's interface are described declaratively as the arguments of `@app.callback` decorator.

-In Dash, the `inputs` and `outputs` of our application are simply the properties of a particular component. 
  
- In this example, our input is the `value` property of the component that has the ID `demo-dropdown`

- Our layout has 2 outputs so we need to create 2 output components.

It is a list with 2 output parameters with component id and property. Here, the component property will be `children` as we have created empty division and passing in `dcc.Graph` (figure) after computation.

Component ids will be `plot1` , `plot2`.



```
@app.callback([Output(component_id='plot1', component_property='children'),
               Output(component_id='plot2', component_property='children')],
               Input(component_id='demo-dropdown', component_property='value'))
```
Once you add the callback decorator  the '`app.layout` will appear as follows

![](images/callbackdec.png)


# TASK 6: Add the call back function.

- Whenever an input property changes, the function that the callback decorator wraps will get called automatically.
- In this case let us define a function `display_selected_drive_charts()` which will be wrapped by our decorator.
-  The function first filters our dataframe `auto_data` by the selected value of the drive-wheels  from the dropdown as follows
-  `auto_data[auto_data['drive-wheels']==value]` . 
-  Next we will group by the `drive-wheels` and `body-style` and calculate the mean `price` of the dataframe.
-  Use the `px.pie()` and `px.bar()` function we will plot the pie chart and bar chart

```
def display_selected_drive_charts(value):
   

   
   filtered_df = auto_data[auto_data['drive-wheels']==value].groupby(['drive-wheels','body-style'],as_index=False). \
            mean()
        
   filtered_df = filtered_df
   
   fig1 = px.pie(filtered_df, values='price', names='body-style', title="Pie Chart")
   fig2 = px.bar(filtered_df, x='body-style', y='price', title='Bar Chart')
    
   return [dcc.Graph(figure=fig1),
            dcc.Graph(figure=fig2) ]
```

- Here for the pie chart we pass the `filtered dataframe` where `values` correspond to `price` and names will be `body-style`
- For the bar chart also we will pass the `filtered dataframe` where x-axis corresponds to `body-style` and y-axis as `price`.
- Finally we return the 2 figure objects `fig1` and `fig2`  in `dcc.Graph` method and finally the plots are displayed as follows


![](images/callbackfunc.png)


# Run the Application

- Firstly, install pandas and dash using the following command

```
pip3 install pandas dash
```
![](images/install_pandas_dash.png)

- Run the python file using the command 

```
python3 Dash_Auto.py
```


- Observe the port number shown in the terminal.

![](images/runapp1.png)

- Click on the `Launch Application` option from the menu bar.

![](images/Launchapp.png)

- Provide the port number and click `OK`

![](images/port.png)

### Congratulations, you have successfully completed your application!

## Author

Lakshmi Holla

## Changelog

| Date       | Version | Changed by | Change Description                   |
| ---------- | ------- | ---------- | ------------------------------------ |
| 20-07-2021 | 1.0     | Lakshmi Holla | Initial version created              |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>







